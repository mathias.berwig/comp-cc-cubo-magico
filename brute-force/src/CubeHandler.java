package brute_force;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("WeakerAccess")
/**
 * Gerenciador de um {@link Cube}. Oferece métodos auxiliares para "embaralhar" e resolver o mesmo.
 */
public class CubeHandler {

    private Cube cube;

    public CubeHandler(Cube cube) {
        this.cube = cube;
    }

    /**
     * Rotaciona randomicamente o cubo para embaralha-lo.
     */
    public void mix() {
        for (int i = 0; i < 1000; i++) {
            randRotate();
        }
    }

    /**
     * Resolve a face frontal do cubo e imprime o resultado.
     */
    public void bruteForceFront() {
        CubeSide cubeSide = cube.front;
        int turnCount = 0;
        final long start = System.currentTimeMillis();

        while (!cubeSide.isSideSame()) {
            randRotate();
            turnCount++;
        }
        final long end = System.currentTimeMillis();

        final long diff = end - start;

        String time = String.format(
                "%d min, %d seg",
                TimeUnit.MILLISECONDS.toMinutes(diff),
                TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff))
        );

        System.out.println("---------- Resolvido ----------");
        System.out.println();
        cubeSide.printSide();
        System.out.println();
        System.out.println("Tempo: " + time);
        System.out.println("Movimentos: " + turnCount);
    }

    /**
     * Resolve todas as faces do cubo e imprime o resultado.
     */
    public void bruteForceFull() {
        final long start = System.currentTimeMillis();

        while (!cube.isSolved()) {
            randRotate();
        }

        final long end = System.currentTimeMillis();

        final long diff = end - start;

        String time = String.format(
                "%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(diff),
                TimeUnit.MILLISECONDS.toSeconds(diff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(diff))
        );

        System.out.println("---------- Resolvido ----------");
        System.out.println();
        cube.printCube();
        System.out.println();
        System.out.println("Tempo: " + time);
    }

    /**
     * @param min início do intervalo.
     * @param max fim do intervalo.
     * @return um número randômico dentro do intervalo informado.
     */
    private int randRange(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    /**
     * Realiza uma rotação randômica no eixo vertical ou horizonal do cubo.
     */
    private void randRotate() {
        switch (randRange(0, 1)) {
            case 0:
                cube.rotateY(randRange(0, 2));
                break;
            case 1:
                cube.rotateX(randRange(0, 2));
                break;
        }
    }
}
