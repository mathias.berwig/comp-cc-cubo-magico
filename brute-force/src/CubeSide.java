package brute_force;

@SuppressWarnings("WeakerAccess")
/**
 * Representa uma face do cubo com 3x3 células. 
 */
public class CubeSide {

    private CubeColor[][] sideData = new CubeColor[3][3];

    public CubeSide(CubeColor color) {
        initCubeSide(color);
    }

    /**
     * Inicializa uma face do cubo.
     *
     * @param color cor da face.
     */
    private void initCubeSide(CubeColor color) {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                sideData[r][c] = color;
            }
        }
    }

    //region Getters
    public String getTopRow() {
        return getRow(0);
    }

    public String getMiddleRow() {
        return getRow(1);
    }

    public String getBottomRow() {
        return getRow(2);
    }

    public String getRow(int r) {
        StringBuilder retString = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            retString.append(sideData[r][i].name());
        }
        return retString.toString();
    }

    public String getLeftColumn() {
        return getColumn(0);
    }

    public String getMiddleColumn() {
        return getColumn(1);
    }

    public String getRightColumn() {
        return getColumn(2);
    }

    public String getColumn(int c) {
        StringBuilder retString = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            retString.append(sideData[i][c].name());
        }
        return retString.toString();
    }
    //endregion

    /**
     * Define uma coluna desta face.
     *
     * @param c    número da coluna.
     * @param data cores (definidas em {@link CubeColor}.
     */
    public void setColumn(int c, String data) {
        for (int i = 0; i < 3; i++) {
            sideData[i][c] = CubeColor.valueOf("" + data.charAt(i));
        }
    }

    /**
     * Define uma linha desta face.
     *
     * @param r    número da linha.
     * @param data cores (definidas em {@link CubeColor}.
     */
    public void setRow(int r, String data) {
        for (int i = 0; i < 3; i++) {
            sideData[r][i] = CubeColor.valueOf("" + data.charAt(i));
        }
    }

    /**
     * @return {@code true} se todas cores desta face forem iguais.
     */
    public boolean isSideSame() {
        CubeColor sideColor = sideData[0][0];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (sideColor != sideData[r][c]) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Imprime esta face do cubo.
     */
    public void printSide() {
        System.out.print("+-----+\n");
        System.out.printf("| %s |\n", getTopRow());
        System.out.printf("| %s |\n", getMiddleRow());
        System.out.printf("| %s |\n", getBottomRow());
        System.out.print("+-----+\n");
    }
}
