package brute_force;

/**
 * Define as cores de uma célula do cubo.
 */
public enum CubeColor {
    W,   // White
    G,   // Green
    R,   // Red
    B,   // Blue
    Y,   // Yellow
    O    // Orange
}
