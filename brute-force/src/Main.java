package brute_force;

/**
 * Classe de inicialização da aplicação.
 */
public class Main {

    public static void main(String[] args) {
        Cube c = new Cube();
        CubeHandler ch = new CubeHandler(c);

        ch.mix();

        c.printCube();
        ch.bruteForceFront();
    }
}
