# Cubo Mágico

## Atividade

Desenvolvimento de um programa que dada uma entrada inicial(arquivo como 3 matrizes 9x9) e/ou interface gráfica. Utilize-se de 2 estratégias para chegar a solução final:

- força bruto: desenvolvida pelo grupo;
- não força bruta: desenvolvida pelo grupo, pode se basear em exemplos (referências os exemplos e explicar a estratéga adotada).

## Relatório

- Mostrar uma ou mais soluções (passo a passo) da resolução do problema;
- Tempo que levou em cada uns dos métodos para cada solução apresentada;
- Podes implementar mais apresentações de resultados.

## Introdução

O Cubo de Rubik é um quebra-cabeça tridimensional inventado em 1974 pelo escultor e professor de arquitetura húngaro Ernõ Rubik. É considerado o brinquedo e jogo de enigma mais vendidos do mundo.

Sua versão original tem 6 faces com 9 blocos cada, sendo que cada bloco possui uma entre 6 cores diferentes. O desafio consiste em permutar os blocos a partir de rotações no eixo vertical ou horizontal desses blocos, a fim de organizar as cores de modo que cada face do cubo seja monocromática.

## Permutações

Segundo Martin Schönert registrou em seu artigo ["Analyzing Rubik's Cube with GAP": the permutation group of Rubik's Cube is examined with GAP computer algebra system](http://www.gap-system.org/Doc/Examples/rubik.html), o cubo possui 8 cantos e 12 arestas, o que resulta em 8! (40.320) possibilidades de movimentos para os cantos do cubo. Cada canto tem três possíveis orientações, embora apenas sete (de oito) possam ser orientadas independentemente; orientação do oitavo (e último) canto depende do anterior, configurando 3^7 (2,187) possibilidades. Existem 12!/2 (239.500.800) maneiras de mover as arestas, restringidas de 12!, pois as extremidades precisam estar em uma permutação par, exatamente onde os cantos estão. Onze arestas podem ser movidas de maneira independente, com o movimento da décima segunda dependendo das anteriores, resultando em 2^11 (2.048) possibilidades. Combinadas, são `8! * 3^7 * (12!/2) * 2^11 = 43.252.003.274.489.586.000`, o que é aproximadamente 43 quintilhões de possibilidades.

## Número Deus

O algoritmo que dá uma solução ótima no sentido de não ser possível outra solução mais curta é conhecido como algoritmo Deus. 30 anos após o lançamento do cubo, foi provado em 2010 (o conhecido Algoritmo de Kociemba, descrito [neste artigo](http://kociemba.org/math/papers/rubik20.pdf)) que todas as posições do cubo podem ser resolvidas com 20 movimentos ou menos.

## Resultados

### Método Força bruta

Foi desenvolvido com base na implementação de [Matthew Nitschke no GitHub](https://github.com/matthewnitschke/rubiks-cube) em Java, pois o grupo enfrentou dificuldades em desenvolver a atividade de modo independente no prazo proposto. O projeto possui 5 classes, sendo elas:

- `Cube`: Representa um cubo de Rubik de 6 faces com 3x3 cores em cada face;
- `CubeColor`: Define as cores de uma célula do cubo;
- `CubeHandler`: Gerenciador de um `Cube`. Oferece métodos auxiliares para "embaralhar" e resolver o mesmo;
- `CubeSide`: Representa uma face do cubo com 3x3 células;
- `Main`: Classe de inicialização da aplicação.

O algoritmo, a partir da classe `Main`, instancia um novo `Cube` e `CubeHandler`, embaralha o cubo e em seguida imprime seu estado na tela (via linha de comando), para então resolvê-lo. Em `Main:13`, é feita a chamada ao método que executa a abordagem de força bruta, sendo `ch.bruteForceFront();` para resolver apenas a face frontal do cubo (e testar o algoritmo), que costuma ser executada de 3 a 5 segundos em um computador moderno - para os testes foi utilizado um Intel Core i7 4790 @ 3.60 Ghz com 16GB de memória. Para resolver todas as faces do cubo, pode-se substituir a linha `13` por `ch.bruteForceFull();`. O tempo de execução varia de acordo com a forma que o cubo foi embaralhado (logo, randomicamente), mas no sistema supracitado, uma execução durou 45 minutos.

#### Abordagem

A abordagem utilizada pelo algoritmo de força bruta é a de permutação, que consiste basicamente em rotacionar uma linha ou coluna do cubo, verificando em seguida se o cubo foi resolvido ou não. Caso não tenha sido resolvido (o que acontece na maioria das vezes), um novo movimento é feito, repetindo o processo até que as cores estejam alinhadas. A decisão sobre qual movimento realizar é feita randomicamente - logo, o processo de resolução é parecido com o de embaralhamento.

### Método Kociemba

O algoritmo de duas fases desenvolvido por Morley Davidson, John Dethridge, Tomas Rokicki e Herbert Kociemba e publicado em 2010 é considerado hoje a forma mais eficiente de se resolver um cubo. A implementação em Java utilizada neste trabalho foi desenvolvida por Dmitry Blackwell e está disponível no [GitHub](https://github.com/dmitryblackwell/RubiksCubeSolver).

![Programa desenvolvido por Dmitry Blackwell](two-phase/img/solve.gif).

A implementação de Blackwell é uma aplicação visual Java desenvolvida usando o framework Swing. O aplicativo tem visual que permite ao usuário inserir manualmente o estado do cubo, definir movimentos que serão executados (similar a um simulador) e, obviamente, resolver o cubo.

A resolução de todas as faces do cubo utilizando o mesmo computador dos testes com o algoritmo de força bruta levou menos de um segundo.

#### Abordagem

A metodologia de duas fases, como o nome sugere, consiste em separar o algoritmo em duas fases - ou dois subgrupos. Na primeira fase, são gerados milhares de movimentos (referidos como `maneuvers` no código) que movem o estado aleatório do cubo até um grupo específico de cubos (ou posições das cores) em que somente oito movimentos são permitidos. A partir desse subgrupo, são executadas somente as operações permitidas, reduzindo drasticamente o número de nós gerados. O algoritmo não é interrompido ao encontrar a primeira solução, continuando até encontrar outras soluções e escolher a mais eficiente - com menos passos.